#pragma once
#include "./Position.h"
#define BEZIER_MAX	100	//曲线最大数量，型值点数量BEZIER_MAX+1
/*
* Bezier曲线类
*/
class Bezier_Curve_Class
{
public:
	Bezier_Curve_Class() :P1(&P2), P4(&P3), pre_point(P1), next_point(P4) {}
	~Bezier_Curve_Class() = default;

	/*生成Bezier曲线*/
	static float Generate_Curve(const Coordinate_Class& current_coor_inworld);
	static void Set_Control_Point(const Coordinate_Class &point) { write_index++; control_point[write_index] = point; }
	static void Init(void);

	static Coordinate_Class target_coor_inworld;	//目标坐标
	static Velocity_Class target_velocity_gain_inworld;	//目标速度各轴比例
	static float target_curve_u;			//目标曲线参数u，该值的整数部分指示第几条曲线，小数部分为Bezier曲线参数
	static float target_curve_length;		//目标曲线参数u，对应的曲线长度
											/*
											* 根据当前世界坐标计算新的曲线参数u
											* 返回true表示计算成功
											* 返回false表示曲线参数超过了范围
											*/
	static bool Cal_Target_Curve_U(const Coordinate_Class current_coor_inworld);

private:
	/*根据输入曲线参数计算对应的曲线长度*/
	float Cal_Length(const float curve_u);
	float curve_length;
	void Generate_Curve(void);

	Coordinate_Class *P1;		//曲线对应的P1点
	Coordinate_Class P2, P3;	//曲线对应的P2，P3点
	Coordinate_Class *P4;		//曲线对应的P4点

	Coordinate_Class *pre_point, *next_point;	//前一条曲线的起点，下一条曲线的终点

	static int write_index, read_index;		//读写下标
	static Coordinate_Class control_point[BEZIER_MAX + 1];	//型值点数目，第一个点(下标为0)为起始点

	struct integration_range_struct
	{
		float left_interval;	//左区间
		float left_value;	//左区间对应的值
							//三阶Bezier曲线导数为2次，单调区间解有2个
		float x_minor;	//较小解	
		float minor_value;	//较小解对应的值
		float x_larger;	//较大解
		float larger_value;	//较大解对应的值
		float right_interval;	//右区间
		float right_value;	//右区间对应的值
	}range[3];	//三个区间，0--x轴，1--y轴，2--angle轴(角度)
	struct axis_length_struct
	{
		float left_length;	//左区间长度
		float middle_length;	//中间区间长度
		float right_length;	//右边区间长度
	}length[3];//三个区间长度，0--x轴，1--y轴，2--angle轴(角度)

			   /*根据abc的值计算区间，abc为二次方程的系数*/
	void Cal_Range(float a, float b, float c, struct integration_range_struct &range);
	int curve_index;	//指示当前是第几条曲线

	typedef enum
	{
		X_axis = 0,	//x轴
		Y_axis,	//y轴
		Angle_axis,	//angle轴
	}AXIS_Enum;
	/*
	* 计算Bezier曲线对应的值
	* angle_eq_length = true 表示角度需等效为长度
	*/
	float func_bezier(const AXIS_Enum axis, const float x, bool angle_eq_length = true);
	/*计算Bezier曲线一阶导数对应的值*/
	float func_diff_bezier(const AXIS_Enum axis, const  float x, bool angle_eq_length = true);

	void Cal_Bezier_Coor(const float curve_u, Coordinate_Class &coor);
	void Cal_Bezier_Velocity(const float curve_u, Velocity_Class &velocity);
};

extern Bezier_Curve_Class bezier_curve[BEZIER_MAX];	//曲线最大数量