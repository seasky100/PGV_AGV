#include "./Bezier.h"
#include "../macros.h"
#include "../parameter_define.h"
#include "../Math/My_Math.h"

/*u的微小量*/
#define DELTA_U 0.0001f

Bezier_Curve_Class bezier_curve[100];

int Bezier_Curve_Class::write_index = 0, Bezier_Curve_Class::read_index = 0;		//读写下标
Coordinate_Class Bezier_Curve_Class::control_point[BEZIER_MAX + 1];	//型值点数目，第一个点(下标为0)为起始点
Coordinate_Class Bezier_Curve_Class::target_coor_inworld;	//目标坐标
Velocity_Class Bezier_Curve_Class::target_velocity_gain_inworld;	//目标速度各轴比例
																	/*曲线参数u*/
float Bezier_Curve_Class::target_curve_u = 0.0f;
float Bezier_Curve_Class::target_curve_length = 0.0f;

float Bezier_Curve_Class::Generate_Curve(const Coordinate_Class & current_coor_inworld)
{
	float sum_length = 0.0f;
	control_point[0] = current_coor_inworld;
	//target_coor_inworld.Clear();
	//target_velocity_gain_inworld.Clear();
	target_curve_u = 0.0f;
	target_curve_length = 0.0f;

	for (int i = 0; i < write_index; i++)
	{
		/*给所有点赋值*/
		bezier_curve[i].curve_index = i;
		bezier_curve[i].P1 = &control_point[i];
		bezier_curve[i].P4 = &control_point[i + 1];
		/*第一条曲线*/
		if (i == 0)
		{
			bezier_curve[i].pre_point = &control_point[i];
		}
		else
		{
			bezier_curve[i].pre_point = &control_point[i - 1];
		}

		/*最后一条曲线*/
		if (i == (write_index - 1))
		{
			bezier_curve[i].next_point = &control_point[i + 1];
		}
		else
		{
			bezier_curve[i].next_point = &control_point[i + 2];
		}

		/*生成曲线*/
		bezier_curve[i].Generate_Curve();
		sum_length += bezier_curve[i].curve_length;
	}

	/*计算目标坐标*/
	bezier_curve[0].Cal_Bezier_Coor(0.0f, target_coor_inworld);
	/*计算目标速度*/
	bezier_curve[0].Cal_Bezier_Velocity(0.0f, target_velocity_gain_inworld);

	return sum_length;
}

void Bezier_Curve_Class::Init(void)
{
	target_coor_inworld.Clear();	//目标坐标
	target_velocity_gain_inworld.Clear();	//目标速度各轴比例
	target_curve_u = 0.0f;			//目标曲线参数u，该值的整数部分指示第几条曲线，小数部分为Bezier曲线参数
	target_curve_length = 0.0f;		//目标曲线参数u，对应的曲线长度
	write_index = 0;
}

/*
* 根据当前世界坐标计算新的曲线参数u
* 返回true表示计算成功
* 返回false表示曲线参数超过了范围
*/
bool Bezier_Curve_Class::Cal_Target_Curve_U(const Coordinate_Class current_coor_inworld)
{
	Coordinate_Class projection_vector;
	Coordinate_Class coor_delta1, coor_delta2;
	Coordinate_Class vector_temp;
	int bezier_index = (int)target_curve_u;

	if (bezier_index >= write_index)
	{
		/*超出了所有曲线*/
		/*插补完成*/
		/*目标坐标*/
		target_coor_inworld = *bezier_curve[write_index - 1].P4;
		/*计算目标速度*/
		//target_velocity_gain_inworld.Clear();
		bezier_curve[write_index - 1].Cal_Bezier_Velocity(1.0f, target_velocity_gain_inworld);
		target_curve_length = 0.0f;
		for (int i = 0; i < write_index; i++)
		{
			target_curve_length += bezier_curve[i].curve_length;
		}
		return false;
	}

	target_curve_u -= (int)target_curve_u;

	/*计算目标坐标*/
	//bezier_curve[bezier_index].Cal_Bezier_Coor(target_curve_u, target_coor_inworld);
	/*计算目标速度*/
	//bezier_curve[bezier_index].Cal_Bezier_Velocity(target_curve_u, target_velocity_gain_inworld);

	/*迭代5次*/
	for (int i = 0; i < 5; i++)
	{
		/*计算待投影向量*/
		projection_vector.x_coor = current_coor_inworld.x_coor - target_coor_inworld.x_coor;
		projection_vector.y_coor = current_coor_inworld.y_coor - target_coor_inworld.y_coor;
		projection_vector.angle_coor = current_coor_inworld.angle_coor - target_coor_inworld.angle_coor;
		projection_vector.Angle2Rad();
		//projection_vector = current_coor_inworld - target_coor_inworld;

		/*计算目标投影向量*/
		//bezier_curve[bezier_index].Cal_Bezier_Coor(target_curve_u - DELTA_U, coor_delta1);
		//bezier_curve[bezier_index].Cal_Bezier_Coor(target_curve_u + DELTA_U, coor_delta2);
		//vector_temp.x_coor = coor_delta2.x_coor - coor_delta1.x_coor;
		//vector_temp.y_coor = coor_delta2.y_coor - coor_delta1.y_coor;
		//vector_temp.angle_coor = coor_delta2.angle_coor - coor_delta1.angle_coor;
		//vector_temp.Angle2Rad();
		//vector_temp = coor_delta2 - coor_delta1;

		/*目标投影向量即为切向量*/
		/*切向量为速度矢量*/
		float X_H_mul_X = target_velocity_gain_inworld.Cal_Norm(2);
		float X_H_mul_y = projection_vector.x_coor*target_velocity_gain_inworld.velocity_x \
			+ projection_vector.y_coor *target_velocity_gain_inworld.velocity_y\
			+ (projection_vector.angle_rad*Parameter_Class::wheel_lx_ly_distance)*(target_velocity_gain_inworld.angular_velocity_mm);

		/*计算投影向量*/
		//float X_H_mul_X = vector_temp.Cal_Norm(2);
		//float X_H_mul_y = projection_vector.x_coor*vector_temp.x_coor \
		//	+ projection_vector.y_coor *vector_temp.y_coor\
		//	+ (projection_vector.angle_rad*Parameter_Class::wheel_lx_ly_distance)*(vector_temp.angle_rad*Parameter_Class::wheel_lx_ly_distance);

		float u_delta = 0.0f;
		/*两者重合*/
		if (F_EQ(X_H_mul_X, 0.0f))
		{
			/*认为当前投影向量即为当前向量*/
			break;
		}
		else
		{
			u_delta = X_H_mul_y / X_H_mul_X;
			//target_curve_u += (u_delta * 2 * DELTA_U);
			target_curve_u += (u_delta);
		}

		/*计算目标坐标*/
		bezier_curve[bezier_index].Cal_Bezier_Coor(target_curve_u, target_coor_inworld);

		/*计算目标投影向量*/
		/*切向量极限为速度矢量*/
		/*计算目标速度*/
		bezier_curve[bezier_index].Cal_Bezier_Velocity(target_curve_u, target_velocity_gain_inworld);
	}

	/*计算目标坐标*/
	//bezier_curve[bezier_index].Cal_Bezier_Coor(target_curve_u, target_coor_inworld);
	/*计算目标速度*/
	//bezier_curve[bezier_index].Cal_Bezier_Velocity(target_curve_u, target_velocity_gain_inworld);

	target_curve_length = 0.0f;
	for (int i = 0; i < bezier_index; i++)
	{
		target_curve_length += bezier_curve[i].curve_length;
	}

	float current_curve_length = bezier_curve[bezier_index].Cal_Length(target_curve_u);
	target_curve_length += current_curve_length;

	target_curve_u += bezier_index;

	return true;
}

float Bezier_Curve_Class::Cal_Length(const float curve_u)
{
	float current_length = 0.0f;
	for (int i = 0; i < 3; i++)
	{
		float current_value = func_bezier((AXIS_Enum)i, curve_u);
		/*在左区间*/
		if (curve_u < range[i].x_minor)
		{
			current_length += ABS(current_value - range[i].left_value);
		}
		/*在中间区间*/
		else if (curve_u < range[i].x_larger)
		{
			current_length += length[i].left_length + ABS(current_value - range[i].minor_value);
		}
		/*在右区间*/
		else if (curve_u < range[i].right_interval)
		{
			current_length += length[i].left_length + length[i].middle_length + ABS(current_value - range[i].larger_value);
		}
		/* 曲线参数超出范围,继续计算*/
		else
		{
			current_length += length[i].left_length + length[i].middle_length + length[i].right_length + ABS(current_value - range[i].right_value);
		}

	}
	return current_length;
}

void Bezier_Curve_Class::Generate_Curve(void)
{
	/*
	* 世界坐标系，AGV坐标系下速度暂存
	* 下标0-起点速度
	* 下标1-终点速度
	*/
	Velocity_Class velocity_inworld[2], velocity_inagv[2];

	/*计算起点速度*/
	velocity_inworld[0].velocity_x = P4->x_coor - pre_point->x_coor;
	velocity_inworld[0].velocity_y = P4->y_coor - pre_point->y_coor;
	velocity_inworld[0].angular_velocity_angle = P4->angle_coor - pre_point->angle_coor;
	velocity_inworld[0].Angle2Rad();
	/*计算终点速度*/
	velocity_inworld[1].velocity_x = next_point->x_coor - P1->x_coor;
	velocity_inworld[1].velocity_y = next_point->y_coor - P1->y_coor;
	velocity_inworld[1].angular_velocity_angle = next_point->angle_coor - P1->angle_coor;
	velocity_inworld[1].Angle2Rad();

	/*计算AGV坐标下的速度*/
	velocity_inworld[0].angular_velocity_mm = velocity_inworld[0].angular_velocity_rad*Parameter_Class::wheel_lx_ly_distance;
	velocity_inworld[1].angular_velocity_mm = velocity_inworld[1].angular_velocity_rad*Parameter_Class::wheel_lx_ly_distance;
	velocity_inagv[0] = Velocity_Class::Absolute_To_Relative(velocity_inworld[0], velocity_inagv[0], *P1);
	velocity_inagv[1] = Velocity_Class::Absolute_To_Relative(velocity_inworld[1], velocity_inagv[1], *P4);

	float k = 1.0f;

	/*缩放各点速度大小*/
	for (int i = 0; i < 2; i++)
	{
		float velocity_abs = velocity_inagv[i].Cal_Norm(1);
		if (velocity_abs > Parameter_Class::wheel_max_line_velocity)
		{
			k = Parameter_Class::wheel_max_line_velocity / velocity_abs;
		}
		else if (velocity_abs < Parameter_Class::wheel_min_line_velocity)
		{
			k = Parameter_Class::wheel_min_line_velocity / velocity_abs;
		}
		else
		{
			k = 1.0f;
		}
		velocity_inworld[i] *= k;
	}

	Coordinate_Class coor_temp;
	float velocity_abs;
	float gain;

	coor_temp.x_coor = P4->x_coor - P1->x_coor;
	coor_temp.y_coor = P4->y_coor - P1->y_coor;
	coor_temp.angle_coor = P4->angle_coor - P1->angle_coor;
	coor_temp.Angle2Rad();
	float distance_temp = coor_temp.Cal_Norm(1);	//计算始末距离

	velocity_abs = velocity_inworld[0].Cal_Norm(1);
	gain = distance_temp / velocity_abs / 4.0f;
	P2.x_coor = P1->x_coor + velocity_inworld[0].velocity_x*gain;
	P2.y_coor = P1->y_coor + velocity_inworld[0].velocity_y*gain;
	P2.angle_coor = P1->angle_coor + velocity_inworld[0].angular_velocity_angle*gain;
	P2.Angle2Rad();

	velocity_abs = velocity_inworld[1].Cal_Norm(1);
	gain = distance_temp / velocity_abs / 4.0f;
	P3.x_coor = P4->x_coor - velocity_inworld[1].velocity_x*gain;
	P3.y_coor = P4->y_coor - velocity_inworld[1].velocity_y*gain;
	P3.angle_coor = P4->angle_coor - velocity_inworld[1].angular_velocity_angle*gain;
	P3.Angle2Rad();

	curve_length = 0.0f;
	float a, b, c;
	for (int i = 0; i < 3; i++)
	{
		a = -3 * (*(&(P1->x_coor) + i)) + 9 * (*(&(P2.x_coor) + i)) - 9 * (*(&(P3.x_coor) + i)) + 3 * (*(&(P4->x_coor) + i));
		b = 6 * (*(&(P1->x_coor) + i)) - 12 * (*(&(P2.x_coor) + i)) + 6 * (*(&(P3.x_coor) + i));
		c = -3 * (*(&(P1->x_coor) + i)) + 3 * (*(&(P2.x_coor) + i));
		Cal_Range(a, b, c, range[i]);
		/*计算长度*/
		range[i].left_value = func_bezier((AXIS_Enum)i, range[i].left_interval);
		range[i].minor_value = func_bezier((AXIS_Enum)i, range[i].x_minor);
		range[i].larger_value = func_bezier((AXIS_Enum)i, range[i].x_larger);
		range[i].right_value = func_bezier((AXIS_Enum)i, range[i].right_interval);

		length[i].left_length = ABS(range[i].left_value - range[i].minor_value);
		length[i].middle_length = ABS(range[i].minor_value - range[i].larger_value);
		length[i].right_length = ABS(range[i].larger_value - range[i].right_value);

		curve_length += length[i].left_length + length[i].middle_length + length[i].right_length;
		//curve_length=curve_length+ABS(func_bezier((AXIS_Enum)i,range[i].left_interval))
	}
	///*计算x轴*/
	//a = -3 * P1->x_coor + 9 * P2.x_coor - 9 * P3.x_coor + 3 * P4->x_coor;
	//b = 6 * P1->x_coor - 12 * P2.x_coor + 6 * P3.x_coor;
	//c = -3 * P1->x_coor + 3 * P2.x_coor;
	//Cal_Range(a, b, c, range[0]);
	///*计算y轴*/
	//a = -3 * P1->y_coor + 9 * P2.y_coor - 9 * P3.y_coor + 3 * P4->y_coor;
	//b = 6 * P1->y_coor - 12 * P2.y_coor + 6 * P3.y_coor;
	//c = -3 * P1->y_coor + 3 * P2.y_coor;
	//Cal_Range(a, b, c, range[1]);
	///*计算angle轴*/
	//a = -3 * P1->angle_coor + 9 * P2.angle_coor - 9 * P3.angle_coor + 3 * P4->angle_coor;
	//b = 6 * P1->angle_coor - 12 * P2.angle_coor + 6 * P3.angle_coor;
	//c = -3 * P1->angle_coor + 3 * P2.angle_coor;
	//Cal_Range(a, b, c, range[2]);



}

void Bezier_Curve_Class::Cal_Range(float a, float b, float c, integration_range_struct & range)
{
	/*范围初值*/
	range.left_interval = 0.0f;
	range.x_minor = 0.0f;
	range.x_larger = 1.0f;
	range.right_interval = 1.0f;

	float delta = b*b - 4 * a*c;
	/*无实数解*/
	if (delta <= 0.0f)
	{
		return;
	}

	/*求两个实数解*/
	delta = sqrtf(delta);
	float x_larger = (-b + delta) / (2 * a);
	float x_minor = (-b - delta) / (2 * a);

	float x_temp;

	/*比较较大解和较小解大小*/
	if (x_larger < x_minor)
	{
		x_temp = x_minor;
		x_minor = x_larger;
		x_larger = x_temp;
	}


	/*如果较小解大于1*/
	if (x_minor >= 1.0f)
	{
		return;
	}
	else if (x_minor >= 0.0f)
	{
		range.x_minor = x_minor;
	}

	if (x_larger <= 0.0f)
	{
		return;
	}
	else if (x_larger <= 1.0f)
	{
		range.x_larger = x_larger;
	}
}

float Bezier_Curve_Class::func_bezier(const AXIS_Enum axis, const  float x, bool angle_eq_length)
{
	float y = 0.0f;
	float b = 1.0f - x;
	y = (*(&(P1->x_coor) + axis))*b*b*b + \
		3 * (*(&(P2.x_coor) + axis))*b*b*x + \
		3 * (*(&(P3.x_coor) + axis))*b*x*x + \
		(*(&(P4->x_coor) + axis))*x*x*x;


	if (angle_eq_length && (axis == AXIS_Enum::Angle_axis))
	{
		y = y / 180.0f*M_PI*Parameter_Class::wheel_lx_ly_distance;
	}
	return y;
}

float Bezier_Curve_Class::func_diff_bezier(const AXIS_Enum axis, const float x, bool angle_eq_length)
{
	float y = 0.0f;
	float P1 = *(&(this->P1->x_coor) + axis);
	float P2 = *(&(this->P2.x_coor) + axis);
	float P3 = *(&(this->P3.x_coor) + axis);
	float P4 = *(&(this->P4->x_coor) + axis);

	y = (-3 * P1 + 9 * P2 - 9 * P3 + 3 * P4)*x*x \
		+ (6 * P1 - 12 * P2 + 6 * P3)*x \
		+ (-3 * P1 + 3 * P2);

	if (angle_eq_length && (axis == AXIS_Enum::Angle_axis))
	{
		y = y / 180.0f*M_PI*Parameter_Class::wheel_lx_ly_distance;
	}

	return y;
}

void Bezier_Curve_Class::Cal_Bezier_Coor(const float curve_u, Coordinate_Class & coor)
{
	coor.x_coor = func_bezier(X_axis, curve_u, false);
	coor.y_coor = func_bezier(Y_axis, curve_u, false);
	coor.angle_coor = func_bezier(Angle_axis, curve_u, false);
	coor.Angle2Rad();
}

void Bezier_Curve_Class::Cal_Bezier_Velocity(const float curve_u, Velocity_Class & velocity)
{
	velocity.velocity_x = func_diff_bezier(X_axis, curve_u, false);
	velocity.velocity_y = func_diff_bezier(Y_axis, curve_u, false);
	velocity.angular_velocity_angle = func_diff_bezier(Angle_axis, curve_u, false);
	velocity.Angle2Rad();
	velocity.angular_velocity_mm = velocity.angular_velocity_rad*Parameter_Class::wheel_lx_ly_distance;
}
